import Loadable from 'react-loadable'
import Loading from './component.loading'

const Component = Loadable({
  loader: () => import('./component'),
  loading: Loading
})

export default Component
